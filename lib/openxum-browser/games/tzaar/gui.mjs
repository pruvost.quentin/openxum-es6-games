"use strict";

import Tzaar from '../../../openxum-core/games/tzaar/index.mjs';
import Graphics from '../../graphics/index.mjs';
import OpenXum from '../../openxum/gui.mjs';

// grid constants definition
const begin_letter = ['A', 'A', 'A', 'A', 'A', 'B', 'C', 'D', 'E'];
const end_letter = ['E', 'F', 'G', 'H', 'I', 'I', 'I', 'I', 'I'];
const begin_number = [1, 1, 1, 1, 1, 2, 3, 4, 5];
const end_number = [5, 6, 7, 8, 9, 9, 9, 9, 9];
const begin_diagonal_letter = ['A', 'A', 'A', 'A', 'A', 'B', 'C', 'D', 'E'];
const end_diagonal_letter = ['E', 'F', 'G', 'H', 'I', 'I', 'I', 'I', 'I'];
const begin_diagonal_number = [5, 4, 3, 2, 1, 1, 1, 1, 1];
const end_diagonal_number = [9, 9, 9, 9, 9, 8, 7, 6, 5];

// enums definition
const letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];

// initial state
const initial_type = [
  // column A
  Tzaar.PieceType.TOTT, Tzaar.PieceType.TOTT, Tzaar.PieceType.TOTT, Tzaar.PieceType.TOTT, Tzaar.PieceType.TOTT,
  // column B
  Tzaar.PieceType.TOTT, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TOTT,
  // column C
  Tzaar.PieceType.TOTT, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TZAAR, Tzaar.PieceType.TZAAR, Tzaar.PieceType.TZAAR, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TOTT,
  // column D
  Tzaar.PieceType.TOTT, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TZAAR, Tzaar.PieceType.TOTT, Tzaar.PieceType.TOTT, Tzaar.PieceType.TZAAR, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TOTT,
  // column E
  Tzaar.PieceType.TOTT, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TZAAR, Tzaar.PieceType.TOTT, Tzaar.PieceType.TOTT, Tzaar.PieceType.TZAAR, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TOTT,
  // column F
  Tzaar.PieceType.TOTT, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TZAAR, Tzaar.PieceType.TOTT, Tzaar.PieceType.TOTT, Tzaar.PieceType.TZAAR, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TOTT,
  // column G
  Tzaar.PieceType.TOTT, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TZAAR, Tzaar.PieceType.TZAAR, Tzaar.PieceType.TZAAR, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TOTT,
  // column H
  Tzaar.PieceType.TOTT, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TZARRA, Tzaar.PieceType.TOTT,
  // column I
  Tzaar.PieceType.TOTT, Tzaar.PieceType.TOTT, Tzaar.PieceType.TOTT, Tzaar.PieceType.TOTT, Tzaar.PieceType.TOTT
];

const initial_color = [
  // column A
  Tzaar.Color.BLACK, Tzaar.Color.BLACK, Tzaar.Color.BLACK, Tzaar.Color.BLACK, Tzaar.Color.WHITE,
  // column B
  Tzaar.Color.WHITE, Tzaar.Color.BLACK, Tzaar.Color.BLACK, Tzaar.Color.BLACK, Tzaar.Color.WHITE, Tzaar.Color.WHITE,
  // column C
  Tzaar.Color.WHITE, Tzaar.Color.WHITE, Tzaar.Color.BLACK, Tzaar.Color.BLACK, Tzaar.Color.WHITE, Tzaar.Color.WHITE, Tzaar.Color.WHITE,
  // column D
  Tzaar.Color.WHITE, Tzaar.Color.WHITE, Tzaar.Color.WHITE, Tzaar.Color.BLACK, Tzaar.Color.WHITE, Tzaar.Color.WHITE, Tzaar.Color.WHITE, Tzaar.Color.WHITE,
  // column E
  Tzaar.Color.WHITE, Tzaar.Color.WHITE, Tzaar.Color.WHITE, Tzaar.Color.WHITE, Tzaar.Color.BLACK, Tzaar.Color.BLACK, Tzaar.Color.BLACK, Tzaar.Color.BLACK,
  // column F
  Tzaar.Color.BLACK, Tzaar.Color.BLACK, Tzaar.Color.BLACK, Tzaar.Color.BLACK, Tzaar.Color.WHITE, Tzaar.Color.BLACK, Tzaar.Color.BLACK, Tzaar.Color.BLACK,
  // column G
  Tzaar.Color.BLACK, Tzaar.Color.BLACK, Tzaar.Color.BLACK, Tzaar.Color.WHITE, Tzaar.Color.WHITE, Tzaar.Color.BLACK, Tzaar.Color.BLACK,
  // column H
  Tzaar.Color.BLACK, Tzaar.Color.BLACK, Tzaar.Color.WHITE, Tzaar.Color.WHITE, Tzaar.Color.WHITE, Tzaar.Color.BLACK,
  // column I
  Tzaar.Color.BLACK, Tzaar.Color.WHITE, Tzaar.Color.WHITE, Tzaar.Color.WHITE, Tzaar.Color.WHITE
];

class Gui extends OpenXum.Gui {
  constructor(c, e, l, g) {
    super(c, e, l, g);

    this._tolerance = 15;
    this._delta_x = 0;
    this._delta_y = 0;
    this._delta_xy = 0;
    this._offset = 0;
    this._pointerX = -1;
    this._pointerY = -1;

    this._selected_coordinates = null;
    this._selected_piece = null;
    this._selected_capture = false;
    this._selected_make_stack = false;
    this._selected_pass = false;
  }

// public methods
  draw() {
    this._compute_deltas();
    this._context.lineWidth = 1;

    // background
    this._context.strokeStyle = "#000000";
    this._context.fillStyle = "#ffffff";
    Graphics.board.draw_round_rect(this._context, 0, 0, this._canvas.width, this._canvas.height, 17, true, true);

    // grid
    this._draw_grid();
    this._draw_coordinates();

    // state
    this._draw_state();

    //intersection
    this._show_intersection();

    if (this._engine.phase() === Tzaar.Phase.CHOOSE || this._engine.phase() === Tzaar.Phase.SECOND_CAPTURE || this._engine.phase() === Tzaar.Phase.MAKE_STRONGER) {
      this._draw_choice();
    }

    if ((this._engine.phase() === Tzaar.Phase.FIRST_MOVE || this._engine.phase() === Tzaar.Phase.CAPTURE ||
      this._engine.phase() === Tzaar.Phase.SECOND_CAPTURE) && this._selected_piece && this._selected_piece.is_valid()) {
      this._draw_possible_capture();
    }

    if (this._engine.phase() === Tzaar.Phase.MAKE_STRONGER && this._selected_piece && this._selected_piece.is_valid()) {
      this._draw_possible_make_stack();
    }
  }

  get_move() {
    let move = null;

    if (this._engine.phase() === Tzaar.Phase.FIRST_MOVE) {
      move = new Tzaar.Move(Tzaar.MoveType.FIRST_MOVE, this._engine.current_color(), this._selected_piece, this._selected_coordinates);
    } else if (this._engine.phase() === Tzaar.Phase.CAPTURE) {
      move = new Tzaar.Move(Tzaar.MoveType.CAPTURE, this._engine.current_color(), this._selected_piece, this._selected_coordinates);
    } else if (this._engine.phase() === Tzaar.Phase.CHOOSE) {
      move = new Tzaar.Move(Tzaar.MoveType.CHOOSE, this._engine.current_color(), null, null, this._selected_capture ? Tzaar.Phase.SECOND_CAPTURE : this._selected_make_stack ? Tzaar.Phase.MAKE_STRONGER : Tzaar.Phase.PASS);
    } else if (this._engine.phase() === Tzaar.Phase.SECOND_CAPTURE) {
      move = new Tzaar.Move(Tzaar.MoveType.SECOND_CAPTURE, this._engine.current_color(), this._selected_piece, this._selected_coordinates);
    } else if (this._engine.phase() === Tzaar.Phase.MAKE_STRONGER) {
      move = new Tzaar.Move(Tzaar.MoveType.MAKE_STRONGER, this._engine.current_color(), this._selected_piece, this._selected_coordinates);
    }
    return move;
  }

  is_animate() {
    return false;
  }

  is_remote() {
    return false;
  }

  move(move, color) {
    this._manager.play();
    // TODO !!!!!
  }

  set_canvas(c) {
    super.set_canvas(c);

    this._canvas.addEventListener("click", (e) => {
      this._on_click(e);
    });
    this._canvas.addEventListener("mousemove", (e) => {
      this._on_move(e);
    });

    this.draw();
  }

  unselect() {
    if (this._engine.phase() === Tzaar.Phase.FIRST_MOVE || this._engine.phase() === Tzaar.Phase.CAPTURE || this._engine.phase() === Tzaar.Phase.CHOOSE) {
      this._selected_capture = false;
      this._selected_make_stack = false;
      this._selected_pass = false;
    }
    this._selected_coordinates = null;
    this._selected_piece = null;
  }

  // private methods
  _compute_coordinates(letter, number) {
    return [this._offset + (letter - 'A'.charCodeAt(0)) * this._delta_x,
      7 * this._delta_y + this._delta_xy * (letter - 'A'.charCodeAt(0)) - (number - 1) * this._delta_y];
  }

  _compute_deltas() {
    this._offset = 30;
    this._delta_x = (this._width - 2 * this._offset) / 9.0;
    this._delta_y = this._delta_x;
    this._delta_xy = this._delta_y / 2;
    this._offset = (this._width - 8 * this._delta_x) / 2;
  }

  _compute_letter(x, y) {
    let index = Math.floor((x - this._offset) / this._delta_x);
    let x_ref = this._offset + this._delta_x * index;
    let x_ref_2 = this._offset + this._delta_x * (index + 1);
    let _letter = 'X';

    if (x < this._offset) {
      _letter = 'A';
    } else if (x <= x_ref + this._delta_x / 2 && x >= x_ref && x <= x_ref + this._tolerance) {
      _letter = letters[index];
    } else if (x > x_ref + this._delta_x / 2 && x >= x_ref_2 - this._tolerance) {
      _letter = letters[index + 1];
    }
    return _letter;
  }

  _compute_number(x, y) {
    let pt = this._compute_coordinates('A'.charCodeAt(0), 1);

    // translation to A1 and rotation
    let X = x - pt[0];
    let Y = y - pt[1];
    let sin_alpha = 1.0 / Math.sqrt(5);
    let cos_alpha = 2.0 * sin_alpha;

    let x2 = Math.floor((X * sin_alpha - Y * cos_alpha) + pt[0]);
    let delta_x2 = Math.floor(this._delta_x * cos_alpha);

    let index = Math.floor((x2 - this._offset) / delta_x2);
    let x_ref = Math.floor(this._offset + delta_x2 * index);
    let x_ref_2 = Math.floor(this._offset + delta_x2 * (index + 1));

    let _number = -1;

    if (x2 > 0 && x2 < this._offset) {
      _number = 1;
    } else if (x2 <= x_ref + delta_x2 / 2 && x2 >= x_ref && x2 <= x_ref + this._tolerance) {
      _number = index + 1;
    } else if (x2 > x_ref + delta_x2 / 2 && x2 >= x_ref_2 - this._tolerance) {
      _number = index + 2;
    }
    return _number;
  }

  _compute_pointer(x, y) {
    let change = false;
    let letter = this._compute_letter(x, y);

    if (letter !== 'X') {
      let number = this._compute_number(x, y);

      if (number !== -1) {
        if (this._engine.exist_intersection(letter, number)) {
          let pt = this._compute_coordinates(letter.charCodeAt(0), number);

          this._pointerX = pt[0];
          this._pointerY = pt[1];
          change = true;
        } else {
          this._pointerX = this._pointerY = -1;
          change = true;
        }
      } else {
        if (this._pointerX !== -1) {
          this._pointerX = this._pointerY = -1;
          change = true;
        }
      }
    } else {
      if (this._pointerX !== -1) {
        this._pointerX = this._pointerY = -1;
        change = true;
      }
    }
    return change;
  }

  _draw_button(x, y, text, select) {
    const height = 25;
    const width = 100;

    if (select) {
      this._context.strokeStyle = "#ff0000";
      this._context.fillStyle = "#ff0000";
    } else {
      this._context.strokeStyle = "#c0c0c0";
      this._context.fillStyle = "#c0c0c0";
    }
    this._context.beginPath();
    this._context.fillRect(x, y, width, height);
    this._context.strokeRect(x, y, width, height);
    this._context.closePath();

    this._context.strokeStyle = "#000000";
    this._context.fillStyle = "#000000";
    this._context.font = "bold 16px _sans";
    this._context.textBaseline = "top";

    const textWidth = this._context.measureText(text).width;

    this._context.fillText(text, x + (width - textWidth) / 2, y + 3);

    this._context.strokeStyle = "#000000";
    this._context.beginPath();
    this._context.strokeRect(x, y, width, height);
    this._context.closePath();
  }

  _draw_choice() {
    const x = (this._width - (2 * 150 + 100)) / 2;

    this._draw_button(x, 3, "capture", this._selected_capture);
    this._draw_button(x + 150, 3, "make stack", this._selected_make_stack);
    this._draw_button(x + 300, 3, "pass", this._selected_pass);
  }

  _draw_coordinates() {
    let pt;

    this._context.strokeStyle = "#000000";
    this._context.fillStyle = "#000000";
    this._context.font = "16px _sans";
    this._context.textBaseline = "top";
    // letters
    for (let l = 'A'.charCodeAt(0); l < 'J'.charCodeAt(0); ++l) {
      pt = this._compute_coordinates(l, begin_number[l - 'A'.charCodeAt(0)]);
      pt[1] += 5;
      this._context.fillText(String.fromCharCode(l), pt[0], pt[1]);
    }

    // numbers
    this._context.textBaseline = "bottom";
    for (let n = 1; n < 10; ++n) {
      pt = this._compute_coordinates(begin_letter[n - 1].charCodeAt(0), n);
      pt[0] -= 15 + (n > 9 ? 5 : 0);
      pt[1] -= 3;
      this._context.fillText(n.toString(), pt[0], pt[1]);
    }
  }

  _draw_grid() {
    let _pt_begin;
    let _pt_end;

    this._context.lineWidth = 1;
    this._context.strokeStyle = "#000000";
    this._context.fillStyle = "#ffffff";

    for (let l = 'A'.charCodeAt(0); l < 'J'.charCodeAt(0); ++l) {
      const index = l - 'A'.charCodeAt(0);

      _pt_begin = this._compute_coordinates(l, begin_number[index]);
      _pt_end = this._compute_coordinates(l, end_number[index]);
      if (l === 'E'.charCodeAt(0)) {
        const _pt_3 = this._compute_coordinates('E'.charCodeAt(0), 4);
        const _pt_4 = this._compute_coordinates('E'.charCodeAt(0), 6);

        this._context.moveTo(_pt_begin[0], _pt_begin[1]);
        this._context.lineTo(_pt_3[0], _pt_3[1]);
        this._context.moveTo(_pt_4[0], _pt_4[1]);
        this._context.lineTo(_pt_end[0], _pt_end[1]);
      } else {
        this._context.moveTo(_pt_begin[0], _pt_begin[1]);
        this._context.lineTo(_pt_end[0], _pt_end[1]);
      }
    }

    for (let n = 1; n < 10; ++n) {
      _pt_begin = this._compute_coordinates(begin_letter[n - 1].charCodeAt(0), n);
      _pt_end = this._compute_coordinates(end_letter[n - 1].charCodeAt(0), n);
      if (n === 5) {
        const _pt_3 = this._compute_coordinates('D'.charCodeAt(0), 5);
        const _pt_4 = this._compute_coordinates('F'.charCodeAt(0), 5);

        this._context.moveTo(_pt_begin[0], _pt_begin[1]);
        this._context.lineTo(_pt_3[0], _pt_3[1]);
        this._context.moveTo(_pt_4[0], _pt_4[1]);
        this._context.lineTo(_pt_end[0], _pt_end[1]);
      } else {
        this._context.moveTo(_pt_begin[0], _pt_begin[1]);
        this._context.lineTo(_pt_end[0], _pt_end[1]);
      }
    }

    for (let i = 0; i < 9; ++i) {
      _pt_begin = this._compute_coordinates(begin_diagonal_letter[i].charCodeAt(0),
        begin_diagonal_number[i]);
      _pt_end = this._compute_coordinates(end_diagonal_letter[i].charCodeAt(0),
        end_diagonal_number[i]);
      if (begin_diagonal_letter[i] === 'A' && begin_diagonal_number[i] === 1) {
        const _pt_3 = this._compute_coordinates('D'.charCodeAt(0), 4);
        const _pt_4 = this._compute_coordinates('F'.charCodeAt(0), 6);

        this._context.moveTo(_pt_begin[0], _pt_begin[1]);
        this._context.lineTo(_pt_3[0], _pt_3[1]);
        this._context.moveTo(_pt_4[0], _pt_4[1]);
        this._context.lineTo(_pt_end[0], _pt_end[1]);
      } else {
        this._context.moveTo(_pt_begin[0], _pt_begin[1]);
        this._context.lineTo(_pt_end[0], _pt_end[1]);
      }
    }
    this._context.stroke();
  }

  _draw_piece(x, y, color, type, selected) {
    const width = this._delta_x;

    if (selected) {
      this._context.strokeStyle = "#00ff00";
      this._context.fillStyle = "#00ff00";
      this._context.lineWidth = this._delta_x / 5;
      this._context.beginPath();
      this._context.arc(x, y, width / 3, 0.0, 2 * Math.PI);
      this._context.closePath();
      this._context.fill();
      this._context.stroke();
    } else {
      if (type === Tzaar.PieceType.TZAAR) {
        if (color === Tzaar.Color.BLACK) {
          this._context.strokeStyle = "#000000";
          this._context.fillStyle = "#000000";
        } else {
          this._context.strokeStyle = "#ffffff";
          this._context.fillStyle = "#ffffff";
        }
        this._context.beginPath();
        this._context.arc(x, y, width * (1. / 3 + 1. / 10), 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.fill();

        this._context.strokeStyle = "#000000";
        this._context.lineWidth = 1;
        this._context.beginPath();
        this._context.arc(x, y, width * (1. / 3 + 1. / 10), 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.stroke();

        if (color === Tzaar.Color.WHITE) {
          this._context.strokeStyle = "#D2691E";
          this._context.fillStyle = "#D2691E";
        } else {
          this._context.strokeStyle = "#ffffff";
          this._context.fillStyle = "#ffffff";
        }
        this._context.lineWidth = width * 1. / 10;
        this._context.beginPath();
        this._context.arc(x, y, width * 2 * (1. / 3 + 1. / 10) / 3, 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.stroke();
        this._context.beginPath();
        this._context.arc(x, y, width * (1. / 3 - 1. / 10) / 3, 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.fill();
        this._context.stroke();
      } else if (type === Tzaar.PieceType.TZARRA) {
        if (color === Tzaar.Color.BLACK) {
          this._context.strokeStyle = "#000000";
          this._context.fillStyle = "#000000";
        } else {
          this._context.strokeStyle = "#ffffff";
          this._context.fillStyle = "#ffffff";
        }
        this._context.beginPath();
        this._context.arc(x, y, width * (1. / 3 + 1. / 10), 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.fill();

        this._context.strokeStyle = "#000000";
        this._context.lineWidth = 1;
        this._context.beginPath();
        this._context.arc(x, y, width * (1. / 3 + 1. / 10), 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.stroke();

        if (color === Tzaar.Color.WHITE) {
          this._context.strokeStyle = "#D2691E";
          this._context.fillStyle = "#D2691E";
        } else {
          this._context.strokeStyle = "#ffffff";
          this._context.fillStyle = "#ffffff";
        }

        this._context.lineWidth = 1;
        this._context.beginPath();
        this._context.arc(x, y, width * 2 * (1. / 3 + 1. / 10) / 3, 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.stroke();

        this._context.lineWidth = 1;
        this._context.beginPath();
        this._context.arc(x, y, width * (1. / 3 + 1. / 10) / 3, 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.fill();
        this._context.stroke();
      } else { // TOTT
        if (color === Tzaar.Color.BLACK) {
          this._context.strokeStyle = "#000000";
          this._context.fillStyle = "#000000";
        } else {
          this._context.strokeStyle = "#ffffff";
          this._context.fillStyle = "#ffffff";
        }
        this._context.beginPath();
        this._context.arc(x, y, width * (1. / 3 + 1. / 10), 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.fill();

        this._context.strokeStyle = "#000000";
        this._context.lineWidth = 1;
        this._context.beginPath();
        this._context.arc(x, y, width * (1. / 3 + 1. / 10), 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.stroke();

        if (color === Tzaar.Color.WHITE) {
          this._context.strokeStyle = "#000000";
          this._context.fillStyle = "#000000";
        } else {
          this._context.strokeStyle = "#ffffff";
          this._context.fillStyle = "#ffffff";
        }
        this._context.lineWidth = 1;
        this._context.beginPath();
        this._context.arc(x, y, width * 2 * (1. / 3 + 1. / 10) / 3, 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.stroke();
        this._context.beginPath();
        this._context.arc(x, y, width * (1. / 3 - 1. / 10) / 3, 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.stroke();
      }
    }
  }

  _draw_possible_capture() {
    const list = this._engine.get_possible_capture(this._selected_piece);

    for (let index in list) {
      const pt = this._compute_coordinates(list[index].letter().charCodeAt(0), list[index].number());

      this._context.lineWidth = 4;
      this._context.strokeStyle = "#0000ff";
      this._context.fillStyle = "#0000ff";
      this._context.beginPath();
      this._context.arc(pt[0], pt[1], this._delta_x * (1. / 3 + 1. / 10), 0.0, 2 * Math.PI);
      this._context.closePath();
      this._context.stroke();
    }
  }

  _draw_possible_make_stack() {
    const list = this._engine.get_possible_make_stack(this._selected_piece);

    for (let index in list) {
      const pt = this._compute_coordinates(list[index].letter().charCodeAt(0), list[index].number());

      this._context.lineWidth = 4;
      this._context.strokeStyle = "#0000ff";
      this._context.fillStyle = "#0000ff";
      this._context.beginPath();
      this._context.arc(pt[0], pt[1], this._delta_x * (1. / 3 + 1. / 10), 0.0, 2 * Math.PI);
      this._context.closePath();
      this._context.stroke();
    }
  }

  _draw_state() {
    for (let index in this._engine.get_intersections()) {
      const intersection = this._engine.get_intersections()[index];

      if (intersection.state() === Tzaar.State.NO_VACANT) {
        const pt = this._compute_coordinates(intersection.letter().charCodeAt(0), intersection.number());
        const n = intersection.size();

        this._draw_piece(pt[0], pt[1], intersection.color(), intersection.type(), this._selected_piece && this._selected_piece.is_valid() && intersection.coordinates().hash() === this._selected_piece.hash());
        if (n > 1) {
          let text;
          let shift_x = 0;

          this._context.font = "10px _sans";
          this._context.textBaseline = "top";
          if (n < 10) {
            text = n.toString();
            shift_x = 3;
          } else {
            text = '1' + (n - 10).toString();
            shift_x = 8;
          }
          this._context.fillStyle = "#ffff00";
          this._context.beginPath();
          this._context.fillRect(pt[0] - 10, pt[1] - this._delta_y * (1.0 / 3 + 1.0 / 10), 20, 15);
          this._context.closePath();

          this._context.fillStyle = "#000000";
          this._context.fillText(text, pt[0] - shift_x, pt[1] - this._delta_y * (1.0 / 3 + 1.0 / 10));
        }
      }
    }
  }

  _get_click_position(e) {
    let rect = this._canvas.getBoundingClientRect();

    return {x: (e.clientX - rect.left) * this._scaleX, y: (e.clientY - rect.top) * this._scaleY};
  }

  _on_click(event) {
    if (this._engine.current_color() === this._color || this._gui) {
      const pos = this._get_click_position(event);

      if (this._engine.phase() !== Tzaar.Phase.CHOOSE) {
        const letter = this._compute_letter(pos.x, pos.y);

        if (letter !== 'X') {
          const number = this._compute_number(pos.x, pos.y);

          if (number !== -1) {
            if (letter !== 'E' || number !== 5) {
              let ok = false;

              if (this._engine.phase() === Tzaar.Phase.FIRST_MOVE) {
                const intersection = this._engine.get_intersection(letter, number);

                if (this._selected_piece && this._selected_piece.is_valid() && intersection.state() === Tzaar.State.NO_VACANT &&
                  intersection.color() !== this._engine.current_color()) {
                  this._selected_coordinates = new Tzaar.Coordinates(letter, number);
                  ok = true;
                } else if (intersection.color() === this._engine.current_color()) {
                  this._selected_piece = new Tzaar.Coordinates(letter, number);
                  this._manager.redraw();
                }
              } else if (this._engine.phase() === Tzaar.Phase.CAPTURE || this._engine.phase() === Tzaar.Phase.SECOND_CAPTURE) {
                if (this._selected_piece && this._selected_piece.is_valid() && this._engine.verify_capture(this._selected_piece, new Tzaar.Coordinates(letter, number))) {
                  this._selected_coordinates = new Tzaar.Coordinates(letter, number);
                  ok = true;
                } else if (this._engine.get_intersection(letter, number).color() === this._engine.current_color()) {
                  this._selected_piece = new Tzaar.Coordinates(letter, number);
                  this._manager.redraw();
                }
              } else if (this._engine.phase() === Tzaar.Phase.MAKE_STRONGER) {
                const intersection = this._engine.get_intersection(letter, number);

                if (this._selected_piece && this._selected_piece.is_valid() && intersection.state() === Tzaar.State.NO_VACANT &&
                  intersection.color() === this._engine.current_color()) {
                  this._selected_coordinates = new Tzaar.Coordinates(letter, number);
                  ok = true;
                } else if (intersection.color() === this._engine.current_color()) {
                  this._selected_piece = new Tzaar.Coordinates(letter, number);
                  this._manager.redraw();
                }
              }
              if (ok) {
                this._manager.play();
              }
            }
          }
        }
      } else {
        const x = (this._width - (2 * 150 + 100)) / 2;

        this._selected_capture = pos.x >= x && pos.x <= x + 100 && pos.y >= 3 && pos.y <= 28;
        this._selected_make_stack = pos.x >= x + 150 && pos.x <= x + 250 && pos.y >= 3 && pos.y <= 28;
        this._selected_pass = pos.x >= x + 300 && pos.x <= x + 400 && pos.y >= 3 && pos.y <= 28;
        if (this._selected_capture || this._selected_make_stack || this._selected_pass) {
          this._manager.play();
        }
      }
    }
  }

  _on_move(event) {
    if (this._engine.current_color() === this._color || this._gui) {
      let pos = this._get_click_position(event);
      let letter = this._compute_letter(pos.x, pos.y);

      if (letter !== 'X') {
        let number = this._compute_number(pos.x, pos.y);

        if (number !== -1) {
          if (this._compute_pointer(pos.x, pos.y)) {
            this._manager.redraw();
          }
        }
      }
    }
  }

  _show_intersection() {
    if (this._pointerX !== -1 && this._pointerY !== -1) {
      this._context.fillStyle = "#0000ff";
      this._context.strokeStyle = "#0000ff";
      this._context.lineWidth = 1;
      this._context.beginPath();
      this._context.arc(this._pointerX, this._pointerY, 5, 0.0, 2 * Math.PI);
      this._context.closePath();
      this._context.fill();
      this._context.stroke();
    }
  }
}

export default Gui;