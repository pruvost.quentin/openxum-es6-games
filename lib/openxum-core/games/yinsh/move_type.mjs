"use strict";

const MoveType = {PUT_RING: 0, PUT_MARKER: 1, MOVE_RING: 2, REMOVE_ROW: 3, REMOVE_RING: 4};

export default MoveType;